% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 917.339813229079255 ; 921.477288832883914 ];

%-- Principal point:
cc = [ 491.318941202940039 ; 373.521806454514206 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.043810714277898 ; -0.031291835876954 ; -0.022565370725382 ; 0.004459749702609 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 10.473996825152902 ; 10.572261502738664 ];

%-- Principal point uncertainty:
cc_error = [ 10.959635894115809 ; 11.713549210586816 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.023223969032041 ; 0.051096555455826 ; 0.003913119916188 ; 0.003478894715897 ; 0.000000000000000 ];

%-- Image size:
nx = 756;
ny = 1008;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.947059e+00 ; 1.872001e+00 ; -2.540240e-01 ];
Tc_1  = [ -9.227606e-01 ; -3.447208e-01 ; 3.026638e+00 ];
omc_error_1 = [ 9.759955e-03 ; 1.077535e-02 ; 2.021568e-02 ];
Tc_error_1  = [ 3.636612e-02 ; 3.920313e-02 ; 3.461272e-02 ];

%-- Image #2:
omc_2 = [ 1.909861e+00 ; 1.882177e+00 ; 3.533913e-01 ];
Tc_2  = [ -7.632538e-01 ; -1.941815e-01 ; 2.747271e+00 ];
omc_error_2 = [ 1.244239e-02 ; 9.096928e-03 ; 2.124618e-02 ];
Tc_error_2  = [ 3.304091e-02 ; 3.563814e-02 ; 3.522357e-02 ];

%-- Image #3:
omc_3 = [ -2.060363e+00 ; -2.104785e+00 ; 3.552608e-01 ];
Tc_3  = [ -5.817335e-01 ; 2.335119e-01 ; 2.901090e+00 ];
omc_error_3 = [ 1.142668e-02 ; 1.388581e-02 ; 2.499996e-02 ];
Tc_error_3  = [ 3.495555e-02 ; 3.764413e-02 ; 3.245879e-02 ];

%-- Image #4:
omc_4 = [ -2.480627e+00 ; 3.218755e-01 ; 3.824365e-01 ];
Tc_4  = [ -7.204690e-01 ; 8.212754e-01 ; 3.222617e+00 ];
omc_error_4 = [ 1.347036e-02 ; 7.643355e-03 ; 1.724231e-02 ];
Tc_error_4  = [ 3.958271e-02 ; 4.179828e-02 ; 3.173269e-02 ];

%-- Image #5:
omc_5 = [ 1.645161e+00 ; 1.495427e+00 ; -7.234139e-01 ];
Tc_5  = [ -9.268312e-01 ; -3.130390e-02 ; 3.283133e+00 ];
omc_error_5 = [ 9.094575e-03 ; 1.109726e-02 ; 1.448185e-02 ];
Tc_error_5  = [ 3.918994e-02 ; 4.255653e-02 ; 3.362845e-02 ];

%-- Image #6:
omc_6 = [ NaN ; NaN ; NaN ];
Tc_6  = [ NaN ; NaN ; NaN ];
omc_error_6 = [ NaN ; NaN ; NaN ];
Tc_error_6  = [ NaN ; NaN ; NaN ];

%-- Image #7:
omc_7 = [ 2.976656e+00 ; 3.373876e-02 ; 5.625393e-01 ];
Tc_7  = [ -2.042555e-01 ; 4.827562e-01 ; 1.906041e+00 ];
omc_error_7 = [ 1.312551e-02 ; 5.385787e-03 ; 2.059510e-02 ];
Tc_error_7  = [ 2.331608e-02 ; 2.418935e-02 ; 2.302912e-02 ];

%-- Image #8:
omc_8 = [ 2.996339e+00 ; -1.156177e-02 ; -1.894926e-01 ];
Tc_8  = [ -7.309324e-01 ; 5.470786e-01 ; 2.549938e+00 ];
omc_error_8 = [ 1.653015e-02 ; 3.229528e-03 ; 2.127987e-02 ];
Tc_error_8  = [ 3.063436e-02 ; 3.303660e-02 ; 3.078208e-02 ];

%-- Image #9:
omc_9 = [ 2.536731e+00 ; 4.653225e-02 ; -6.650542e-01 ];
Tc_9  = [ -1.013954e+00 ; 3.603061e-01 ; 2.964372e+00 ];
omc_error_9 = [ 1.370911e-02 ; 7.344266e-03 ; 1.571723e-02 ];
Tc_error_9  = [ 3.520539e-02 ; 3.867542e-02 ; 3.402396e-02 ];

%-- Image #10:
omc_10 = [ 1.586433e+00 ; 1.990423e+00 ; -7.934182e-01 ];
Tc_10  = [ -9.856978e-01 ; -5.202752e-01 ; 3.295961e+00 ];
omc_error_10 = [ 6.351136e-03 ; 1.297569e-02 ; 1.744182e-02 ];
Tc_error_10  = [ 3.917860e-02 ; 4.298121e-02 ; 3.534940e-02 ];

%-- Image #11:
omc_11 = [ 1.419368e+00 ; -2.422251e+00 ; 9.603470e-01 ];
Tc_11  = [ 6.336457e-01 ; -2.298842e-01 ; 2.762039e+00 ];
omc_error_11 = [ 6.590503e-03 ; 1.494622e-02 ; 1.769923e-02 ];
Tc_error_11  = [ 3.281795e-02 ; 3.522641e-02 ; 3.237876e-02 ];

%-- Image #12:
omc_12 = [ -4.578434e-02 ; -2.796416e+00 ; 2.357734e-01 ];
Tc_12  = [ -1.149313e-01 ; -4.889144e-01 ; 2.317354e+00 ];
omc_error_12 = [ 6.094039e-03 ; 1.392097e-02 ; 2.030937e-02 ];
Tc_error_12  = [ 2.794091e-02 ; 2.927912e-02 ; 2.809986e-02 ];

%-- Image #13:
omc_13 = [ -1.038514e-01 ; 2.951175e+00 ; 1.036668e+00 ];
Tc_13  = [ 5.382920e-01 ; -2.056458e-01 ; 2.630322e+00 ];
omc_error_13 = [ 6.723657e-03 ; 1.502119e-02 ; 2.098217e-02 ];
Tc_error_13  = [ 3.127947e-02 ; 3.351890e-02 ; 3.425750e-02 ];

%-- Image #14:
omc_14 = [ -1.091646e-01 ; 2.756544e+00 ; 1.812836e-01 ];
Tc_14  = [ 7.648187e-01 ; -2.238989e-01 ; 4.862668e+00 ];
omc_error_14 = [ 5.878945e-03 ; 1.931578e-02 ; 2.661690e-02 ];
Tc_error_14  = [ 5.827145e-02 ; 6.199148e-02 ; 5.661600e-02 ];

%-- Image #15:
omc_15 = [ -1.491533e-01 ; 2.741077e+00 ; -3.129902e-01 ];
Tc_15  = [ 6.693409e-01 ; -6.164305e-01 ; 5.341826e+00 ];
omc_error_15 = [ 6.939338e-03 ; 1.780893e-02 ; 2.636091e-02 ];
Tc_error_15  = [ 6.357908e-02 ; 6.800829e-02 ; 5.990872e-02 ];

%-- Image #16:
omc_16 = [ -4.911326e-01 ; 3.032966e+00 ; -8.066638e-01 ];
Tc_16  = [ 6.303792e-02 ; -6.500872e-01 ; 4.593253e+00 ];
omc_error_16 = [ 9.291417e-03 ; 1.376914e-02 ; 2.549464e-02 ];
Tc_error_16  = [ 5.495336e-02 ; 5.817524e-02 ; 5.386945e-02 ];

%-- Image #17:
omc_17 = [ 1.812189e+00 ; 1.746692e+00 ; -5.077150e-03 ];
Tc_17  = [ -6.571157e-01 ; -4.929305e-01 ; 3.105907e+00 ];
omc_error_17 = [ 1.041119e-02 ; 9.083779e-03 ; 1.818784e-02 ];
Tc_error_17  = [ 3.733517e-02 ; 3.969365e-02 ; 3.645938e-02 ];

%-- Image #18:
omc_18 = [ 1.862501e+00 ; 2.050268e+00 ; -5.998404e-01 ];
Tc_18  = [ -1.194143e+00 ; -3.312520e-01 ; 3.500405e+00 ];
omc_error_18 = [ 7.438326e-03 ; 1.386320e-02 ; 2.041269e-02 ];
Tc_error_18  = [ 4.158024e-02 ; 4.598982e-02 ; 3.954661e-02 ];

%-- Image #19:
omc_19 = [ -1.740721e+00 ; -2.351874e+00 ; 7.465100e-01 ];
Tc_19  = [ -1.034646e+00 ; 1.004927e-01 ; 3.302184e+00 ];
omc_error_19 = [ 1.158869e-02 ; 1.265852e-02 ; 2.164231e-02 ];
Tc_error_19  = [ 3.979470e-02 ; 4.334035e-02 ; 3.609706e-02 ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

