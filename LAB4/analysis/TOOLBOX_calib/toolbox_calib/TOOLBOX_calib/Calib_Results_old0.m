% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 917.701751512319788 ; 921.685475822410922 ];

%-- Principal point:
cc = [ 489.660525774675932 ; 374.640045369884433 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.045469269175952 ; -0.029042628585193 ; -0.022033874821828 ; 0.004187083880335 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 10.205007412684560 ; 10.291974927634572 ];

%-- Principal point uncertainty:
cc_error = [ 10.737847738247023 ; 11.435525017752942 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.022742367835816 ; 0.050799633540989 ; 0.003815053312896 ; 0.003405006743210 ; 0.000000000000000 ];

%-- Image size:
nx = 756;
ny = 1008;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.947678e+00 ; 1.873271e+00 ; -2.540707e-01 ];
Tc_1  = [ -9.172030e-01 ; -3.486070e-01 ; 3.028009e+00 ];
omc_error_1 = [ 9.528213e-03 ; 1.053228e-02 ; 1.972764e-02 ];
Tc_error_1  = [ 3.562833e-02 ; 3.826796e-02 ; 3.375556e-02 ];

%-- Image #2:
omc_2 = [ 1.911533e+00 ; 1.883286e+00 ; 3.526193e-01 ];
Tc_2  = [ -7.583264e-01 ; -1.976055e-01 ; 2.749384e+00 ];
omc_error_2 = [ 1.214764e-02 ; 8.884339e-03 ; 2.074761e-02 ];
Tc_error_2  = [ 3.237942e-02 ; 3.479897e-02 ; 3.434584e-02 ];

%-- Image #3:
omc_3 = [ -2.058704e+00 ; -2.103171e+00 ; 3.546797e-01 ];
Tc_3  = [ -5.765087e-01 ; 2.299513e-01 ; 2.902996e+00 ];
omc_error_3 = [ 1.114884e-02 ; 1.355114e-02 ; 2.433148e-02 ];
Tc_error_3  = [ 3.424078e-02 ; 3.674907e-02 ; 3.164855e-02 ];

%-- Image #4:
omc_4 = [ -2.478973e+00 ; 3.222992e-01 ; 3.849436e-01 ];
Tc_4  = [ -7.145251e-01 ; 8.171335e-01 ; 3.225619e+00 ];
omc_error_4 = [ 1.314794e-02 ; 7.469105e-03 ; 1.685705e-02 ];
Tc_error_4  = [ 3.878850e-02 ; 4.081948e-02 ; 3.094239e-02 ];

%-- Image #5:
omc_5 = [ 1.646371e+00 ; 1.494467e+00 ; -7.296322e-01 ];
Tc_5  = [ -9.212309e-01 ; -3.168866e-02 ; 3.286253e+00 ];
omc_error_5 = [ 8.884674e-03 ; 1.087713e-02 ; 1.414702e-02 ];
Tc_error_5  = [ 3.841298e-02 ; 4.156149e-02 ; 3.278282e-02 ];

%-- Image #6:
omc_6 = [ NaN ; NaN ; NaN ];
Tc_6  = [ NaN ; NaN ; NaN ];
omc_error_6 = [ NaN ; NaN ; NaN ];
Tc_error_6  = [ NaN ; NaN ; NaN ];

%-- Image #7:
omc_7 = [ 2.978246e+00 ; 3.339570e-02 ; 5.604754e-01 ];
Tc_7  = [ -2.007106e-01 ; 4.804344e-01 ; 1.907785e+00 ];
omc_error_7 = [ 1.286114e-02 ; 5.262624e-03 ; 2.019785e-02 ];
Tc_error_7  = [ 2.284194e-02 ; 2.362340e-02 ; 2.245840e-02 ];

%-- Image #8:
omc_8 = [ 2.997847e+00 ; -1.122255e-02 ; -1.910826e-01 ];
Tc_8  = [ -7.263389e-01 ; 5.439005e-01 ; 2.552735e+00 ];
omc_error_8 = [ 1.612516e-02 ; 3.144209e-03 ; 2.085636e-02 ];
Tc_error_8  = [ 3.003463e-02 ; 3.226733e-02 ; 3.002241e-02 ];

%-- Image #9:
omc_9 = [ 2.537580e+00 ; 4.773573e-02 ; -6.672317e-01 ];
Tc_9  = [ -1.008702e+00 ; 3.565117e-01 ; 2.967697e+00 ];
omc_error_9 = [ 1.338406e-02 ; 7.180386e-03 ; 1.541780e-02 ];
Tc_error_9  = [ 3.452600e-02 ; 3.777557e-02 ; 3.317488e-02 ];

%-- Image #10:
omc_10 = [ 1.586491e+00 ; 1.992392e+00 ; -7.935119e-01 ];
Tc_10  = [ -9.796584e-01 ; -5.245523e-01 ; 3.297084e+00 ];
omc_error_10 = [ 6.202442e-03 ; 1.270489e-02 ; 1.705061e-02 ];
Tc_error_10  = [ 3.839387e-02 ; 4.195968e-02 ; 3.446825e-02 ];

%-- Image #11:
omc_11 = [ 1.420475e+00 ; -2.422294e+00 ; 9.575891e-01 ];
Tc_11  = [ 6.385612e-01 ; -2.330912e-01 ; 2.761838e+00 ];
omc_error_11 = [ 6.427893e-03 ; 1.460336e-02 ; 1.736228e-02 ];
Tc_error_11  = [ 3.214847e-02 ; 3.439409e-02 ; 3.157150e-02 ];

%-- Image #12:
omc_12 = [ -4.492434e-02 ; -2.795311e+00 ; 2.331099e-01 ];
Tc_12  = [ -1.106229e-01 ; -4.917529e-01 ; 2.317345e+00 ];
omc_error_12 = [ 5.939970e-03 ; 1.362866e-02 ; 1.982405e-02 ];
Tc_error_12  = [ 2.736654e-02 ; 2.857773e-02 ; 2.740304e-02 ];

%-- Image #13:
omc_13 = [ -1.029921e-01 ; 2.952292e+00 ; 1.039187e+00 ];
Tc_13  = [ 5.429949e-01 ; -2.088436e-01 ; 2.630026e+00 ];
omc_error_13 = [ 6.585824e-03 ; 1.467525e-02 ; 2.048461e-02 ];
Tc_error_13  = [ 3.063417e-02 ; 3.272114e-02 ; 3.339882e-02 ];

%-- Image #14:
omc_14 = [ -1.086616e-01 ; 2.758147e+00 ; 1.826978e-01 ];
Tc_14  = [ 7.735519e-01 ; -2.298284e-01 ; 4.863482e+00 ];
omc_error_14 = [ 5.733125e-03 ; 1.885968e-02 ; 2.598348e-02 ];
Tc_error_14  = [ 5.708478e-02 ; 6.052854e-02 ; 5.519852e-02 ];

%-- Image #15:
omc_15 = [ -1.491952e-01 ; 2.742474e+00 ; -3.114936e-01 ];
Tc_15  = [ 6.788114e-01 ; -6.228382e-01 ; 5.341629e+00 ];
omc_error_15 = [ 6.774108e-03 ; 1.738863e-02 ; 2.572089e-02 ];
Tc_error_15  = [ 6.228352e-02 ; 6.639418e-02 ; 5.840625e-02 ];

%-- Image #16:
omc_16 = [ -4.921448e-01 ; 3.033886e+00 ; -8.049475e-01 ];
Tc_16  = [ 7.127609e-02 ; -6.555567e-01 ; 4.594116e+00 ];
omc_error_16 = [ 9.095554e-03 ; 1.347125e-02 ; 2.490251e-02 ];
Tc_error_16  = [ 5.383873e-02 ; 5.679995e-02 ; 5.250246e-02 ];

%-- Image #17:
omc_17 = [ 1.813297e+00 ; 1.748046e+00 ; -5.441554e-03 ];
Tc_17  = [ -6.514486e-01 ; -4.967954e-01 ; 3.107074e+00 ];
omc_error_17 = [ 1.017318e-02 ; 8.899427e-03 ; 1.776508e-02 ];
Tc_error_17  = [ 3.657607e-02 ; 3.875034e-02 ; 3.554816e-02 ];

%-- Image #18:
omc_18 = [ 1.862849e+00 ; 2.052063e+00 ; -5.996889e-01 ];
Tc_18  = [ -1.187797e+00 ; -3.358695e-01 ; 3.502311e+00 ];
omc_error_18 = [ 7.268024e-03 ; 1.355643e-02 ; 1.994308e-02 ];
Tc_error_18  = [ 4.074987e-02 ; 4.489721e-02 ; 3.857026e-02 ];

%-- Image #19:
omc_19 = [ -1.738909e+00 ; -2.351052e+00 ; 7.451413e-01 ];
Tc_19  = [ -1.028767e+00 ; 9.620214e-02 ; 3.304718e+00 ];
omc_error_19 = [ 1.132573e-02 ; 1.236159e-02 ; 2.109591e-02 ];
Tc_error_19  = [ 3.898654e-02 ; 4.231974e-02 ; 3.521699e-02 ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

