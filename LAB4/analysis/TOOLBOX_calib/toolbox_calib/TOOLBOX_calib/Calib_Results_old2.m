% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 659.977732404979292 ; 659.334826343329269 ];

%-- Principal point:
cc = [ 304.313527964127786 ; 244.245055446561480 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.259170764035178 ; 0.145389561167561 ; 0.001821166325563 ; 0.000123058745150 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 3.510054650073658 ; 2.169887159374667 ];

%-- Principal point uncertainty:
cc_error = [ 2.053056285445012 ; 4.970412372601470 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.008759789601582 ; 0.039854084812338 ; 0.001668495266285 ; 0.000462440471700 ; 0.000000000000000 ];

%-- Image size:
nx = 640;
ny = 480;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 4;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.657235e+00 ; 1.652569e+00 ; -6.654584e-01 ];
Tc_1  = [ -9.108606e-01 ; -4.368393e-01 ; 4.344416e+00 ];
omc_error_1 = [ 4.170015e-03 ; 3.905914e-03 ; 4.546737e-03 ];
Tc_error_1  = [ 1.351517e-02 ; 3.170641e-02 ; 2.282949e-02 ];

%-- Image #2:
omc_2 = [ 1.851584e+00 ; 1.901724e+00 ; -3.904110e-01 ];
Tc_2  = [ -7.950127e-01 ; -8.203695e-01 ; 3.856044e+00 ];
omc_error_2 = [ 3.154488e-03 ; 3.034790e-03 ; 4.248234e-03 ];
Tc_error_2  = [ 1.205776e-02 ; 2.714766e-02 ; 2.036948e-02 ];

%-- Image #3:
omc_3 = [ 1.744947e+00 ; 2.079092e+00 ; -4.985059e-01 ];
Tc_3  = [ -6.442650e-01 ; -8.983248e-01 ; 3.947055e+00 ];
omc_error_3 = [ 2.476102e-03 ; 3.184094e-03 ; 4.476834e-03 ];
Tc_error_3  = [ 1.234006e-02 ; 2.757155e-02 ; 2.100262e-02 ];

%-- Image #4:
omc_4 = [ 1.831116e+00 ; 2.119099e+00 ; -1.099547e+00 ];
Tc_4  = [ -3.351749e-01 ; -7.974296e-01 ; 3.965046e+00 ];
omc_error_4 = [ 1.763515e-03 ; 5.127110e-03 ; 7.208640e-03 ];
Tc_error_4  = [ 1.242674e-02 ; 2.826547e-02 ; 1.913819e-02 ];

