close all;
clear all;
clc;

%import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')
loop_imu = readtable('big_loop_imu_data.csv');
loop_YPR = readtable('big_loop_YPR_data.csv');
loop_gps = readtable('big_loop_gps_data.csv');
loop_Mag = readtable('big_loop_Mag_data.csv');

utm_easting = loop_gps.field_utmeasting;
utm_northing = loop_gps.field_utmnorthing;

easting_v = diff(utm_easting);
northing_v = diff(utm_northing);
vel_gps = sqrt(easting_v.^2+northing_v.^2);

starting_point = 3600;

imu_accel_x = loop_imu.field_linear_acceleration_x(starting_point:end);
imu_accel_y = loop_imu.field_linear_acceleration_y(starting_point:end);
imu_angular_velocity_z = loop_imu.field_angular_velocity_z(starting_point:end);

figure
subplot(2,1,1)
plot(imu_accel_x)

subplot(2,1,2)
plot(imu_accel_y)
















