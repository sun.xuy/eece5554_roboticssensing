
clear all;
close all;
clc;

% import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')


calib_imu = readtable('three_loops_imu_data.csv');
calib_accel_x = calib_imu.field_linear_acceleration_x;
calib_accel_x_compensated = calib_imu.field_linear_acceleration_x-.58;
% make the accel_x to be the same size of gps

vel_imu = cumtrapz(0.025,calib_accel_x);
vel_imu_compensated = cumtrapz(0.025,calib_accel_x_compensated);

% get GPS easting and northing
calib_gps = readtable('three_loops_gps_data.csv');
easting = calib_gps.field_utmeasting;
northing = calib_gps.field_utmnorthing;
% calculate velocity from GPS

easting_v = diff(easting);
northing_v = diff(northing);
vel_gps = sqrt(easting_v.^2+northing_v.^2);
% condense imu data to the same size as gps
vel_imu_condense = vel_imu(1:40:end);
vel_imu_compensated_condense = vel_imu_compensated(1:40:end);


figure
% plot comparison of forward velocity from gps and  imu
subplot(2,1,1)
plot(vel_gps)
hold on
plot(vel_imu_condense+3.3)
hold off
legend('gps','integrated from accelerator')
xlabel('time[s]')
ylabel('velocity[m/s]')
title('comparison of forward velocity from gps and accelerator')

% plot comparison of forward velocity from gps and  compensated imu
subplot(2,1,2)
plot(vel_gps)
hold on
plot(vel_imu_compensated_condense+3.3)
hold off
legend('gps','integrated from accelerator')
xlabel('time[s]')
ylabel('velocity[m/s]')
title('comparison of forward velocity from gps and compensated accelerator')










