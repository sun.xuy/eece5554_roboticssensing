

close all;
clear all;
clc;

%import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames');

calib_Mag = readtable('three_loops_Mag_data.csv');
calib_Magx = calib_Mag.field_magnetic_field_x;
% length_calib_Magx = length(calib_Magx);
calib_Magy = calib_Mag.field_magnetic_field_y;
% length_calib_Magy = length(calib_Magy);


% filter the raw data 
n = length(calib_Magx);
for i = 2:n

    calib_Magx_filtered(i-1) = (calib_Magx(i-1)+calib_Magx(i))/2;
    calib_Magy_filtered(i-1) = (calib_Magy(i-1)+calib_Magy(i))/2;

end

calib_Magx_filtered(1,end) = calib_Magx_filtered(1,end-1);
calib_Magy_filtered(1,end) = calib_Magy_filtered(1,end-1);

% remove soft iron and hard iron effect
% refer to web: 
% https://www.fierceelectronics.com/components/compensating-for-tilt-hard-iron-and-soft-iron-effects
offset_x = (max(calib_Magx_filtered) + min(calib_Magx_filtered)) / 2;   %-0.0179
offset_y = (max(calib_Magy_filtered) + min(calib_Magy_filtered)) / 2;   %-0.1959

% offset_x = mean(calib_Magx_filtered);
% offset_y = mean(calib_Magy_filtered);

x_h = calib_Magx_filtered-offset_x;
y_h = calib_Magy_filtered-offset_y;
% concatenate Magx and Magy
p_h = [x_h;y_h];

% finds major and minoraxis of eclipse

distance = sqrt(x_h.^2+y_h.^2);
[Max,I_max] = max(distance);
[Min,I_min] = min(distance);

% position of a point that sits on the major axis
x1 = x_h(I_max);
y1 = y_h(I_max);

% length of half of the major axis
r = sqrt(x1^2+y1^2);
% length of half of the minor axis
% q = sqrt(x2^2+y2^2);

q = 0.06;
% ratio
sigma = q/r;
% tilted angle
theta = asin(y1/r);

gamma1 = theta;
% finds rotation matrix
R1 = [cos(gamma1) -sin(gamma1);sin(gamma1) cos(gamma1)];
% concatenate raw data as a matrix
vec = [x_h;y_h];

% data after hard and soft iron cancellation
% matrix is 2xn
% phase 2
p_hr = R1*vec;
% phase 3
tmp = p_hr(1,:).*sigma;
p_hrs = [tmp;p_hr(2,:)];
% phase 4
gamma2 = -theta;
R2 = [cos(gamma2) -sin(gamma2);sin(gamma2) cos(gamma2)];
p_hrsr = R2*p_hrs;

% tracking points from rotation to soft iron 
x_hr = p_hr(1,I_max);
y_hr = p_hr(2,I_max);

x_hrs = p_hrs(1,I_max);
y_hrs = p_hrs(2,I_max);

x_hrsr = p_hrsr(1,I_max);
y_hrsr = p_hrsr(2,I_max);

Mag_corrected = p_hrsr;

%%

% plot a series of data processing
% flow diagram of the removal of the hard and soft iron
figure;
subplot(2,3,1)

plot(calib_Magx,calib_Magy);
hold on 
plot(0,0,'o')
hold off
axis equal
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
title('raw data')

subplot(2,3,2)
plot(calib_Magx_filtered,calib_Magy_filtered);
hold on 
plot(0,0,'o')
hold off
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
axis equal
title('filtered data')

subplot(2,3,3)
plot(x_h,y_h);
hold on 
plot(x_h(I_max),y_h(I_max),'x')
plot(0,0,'o')
hold off
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
axis equal
title('phase 1')
% title('hard iron(x_ h)')

% soft
subplot(2,3,4)
plot(p_hr(1,:),p_hr(2,:))
hold on 
plot(0,0,'o')
plot(x_hr,y_hr,'x')
hold off
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
axis equal
% title('rotation')
title('phase 2')

subplot(2,3,5)
plot(p_hrs(1,:),p_hrs(2,:))
hold on 
plot(0,0,'o')
plot(x_hrs,y_hrs,'x')
hold off
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
axis equal
% title('rotation before soft iron')
title('phase 3')

subplot(2,3,6)
scatter(Mag_corrected(1,:),Mag_corrected(2,:),1);
hold on 
plot(0,0,'o')
plot(x_hrsr,y_hrsr,'x')
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
hold off
axis equal
% title('soft iron')
title('phase 4')


% comparison of the raw data and after processed
figure;
scatter(Mag_corrected(1,:),Mag_corrected(2,:),1);
hold on 
scatter(calib_Magx,calib_Magy,1)
hold off
title('calibration')
xlabel('magnetometer[Guass]')
ylabel('magnetometer[Guass]')
legend('corrected Magnetometer','raw Magnetometer')
axis equal


%%

% calculate the yaw angle from magnetometer
% yaw = atan2(-Mag_y,Mag_x)
yaw_Mag_corrected = atan2(-Mag_corrected(2,:),Mag_corrected(1,:));

% load the data from imu
calib_imu = readtable('three_loops_imu_data.csv');
% obtain angular velocity of z axis
z_angular_velocity = calib_imu.field_angular_velocity_z;

% integrated to obtain yaw angle
yaw_integration = 1/40*cumtrapz(z_angular_velocity);

% 0.0773 is offset angle
yaw_integration_wrap = wrapToPi(yaw_integration+0.773);

% yaw angle from corrected magnetometer
% figure;
% plot(yaw_Mag_corrected);
% xlabel('time step[n]')
% ylabel('yaw angle[rad]')
% title('yaw from mag corrected')
% 
% figure;
% plot(yaw_integration_wrap);
% xlabel('time step[n]')
% ylabel('yaw angle[rad]')
% title('yaw from angular velocity integration')

% plot yaw angle comparison from mag & integration
figure;
plot(yaw_Mag_corrected);
hold on
plot(yaw_integration_wrap);
hold off
xlabel('time step[n]')
ylabel('yaw angle[rad]')
legend('yaw from mag corrected','yaw from gyro')
title('yaw angle comparison')



% an used reference:
% https://sites.google.com/site/myimuestimationexperience/filters/complementary-filter

% moving average: lowpass filter
windowSize = 10; 
b = (1/windowSize)*ones(1,windowSize);
a = 1;
yaw_Mag_filtered = filter(b,a,yaw_Mag_corrected);

% high pass filter:yaw_integration_filtered = highpass(yaw_integration_wrap,1e-3,fs);
% yaw data from gyro don't need high pass filter

yaw_integration_filtered = yaw_integration_wrap;


% plot filtered yaw angle comparison
figure;
subplot(1,2,1)
plot(yaw_Mag_corrected)
hold on 
plot(yaw_Mag_filtered,'k');
legend('raw','filtered')
xlabel('time step[n]')
ylabel('yaw angle[rad]')
title('low pass')

subplot(1,2,2)
plot(yaw_integration_wrap)
hold on 
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
plot(yaw_integration_wrap,'k');
legend('raw','filtered')
xlabel('time step[n]')
ylabel('yaw angle[rad]')
title('high pass')

sgtitle('filtered yaw angle comparison')

%%




% choose weight
a = 0.95;
% make the size of each yaw angle same
yaw_integration_filtered_transpose = yaw_integration_filtered';
yaw_integration_filtered_transpose = yaw_integration_filtered_transpose(1:2253);

% apply complimentary filter
% Estimated Angle = a*(Angle integrated by gyro) + (1-a)*(Angle calculated by magnetometer)
yaw_complimentary = (1-a)*yaw_Mag_filtered+a*yaw_integration_filtered_transpose;

% yaw angle from imu directly
loop_YPR = readtable('three_loops_YPR_data.csv');
yaw_degree = loop_YPR.field_Yaw;
yaw_YPR = yaw_degree*pi/180;
yaw_YPR_wrap = wrapToPi(yaw_YPR+2.95);



% figure;
% plot(yaw_complimentary)
% hold on
% plot(yaw_YPR_wrap)
% hold off

figure;
plot(yaw_Mag_corrected);
title('yaw angle from corrected magnetometer')
xlabel('time step[n]')
ylabel('yaw angle[rad]')
% 
figure;
plot(yaw_Mag_corrected);
hold on 
plot(yaw_integration_wrap);
plot(yaw_complimentary)
hold off
legend('mag','integrated','complimentary')
title('comparison of yaw angle from Mag, integration and complimentary')

% calculate the yaw angle from magxand magy
% yaw_angle= atan2(-magy,magx)
yaw_Mag_raw = atan2(-calib_Magy,calib_Magx);

% comparison of yaw angle from raw and corrected
figure;
plot(yaw_complimentary);
hold on 
plot(yaw_Mag_raw);
hold off
legend('complimentary','raw')
title('comparison between yaw from raw magnetometer & corrected and complementarily filtered')





