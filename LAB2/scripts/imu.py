#!/usr/bin/env python

import serial
from lab2.msg import imu_data_raw
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
import time
import rospy
from squaternion import euler2quat
import numpy as np
import time

time_step = 0.001



def read_data():
    device = '/dev/ttyUSB0'
    baudrate = 115200
    ser = serial.Serial(device, baudrate)

    rospy.init_node('imu_mag_data_node')
    pub_imu = rospy.Publisher('/imu_data', Imu, queue_size = 20)
    pub_mag = rospy.Publisher('/MagneticField_data', MagneticField, queue_size=20)
    pub_raw = rospy.Publisher('/raw_data', imu_data_raw, queue_size=20)
    ser.readline()
    ser.readline()
    ser.readline()
    ser.readline()

    # while not rospy.is_shutdown():
    while not rospy.is_shutdown():

        data_raw_string = ser.readline()
        print(data_raw_string)
        # data_raw = np.array(data_raw_string.split(','))
        #data_raw = data_raw_string.strip().strip('\x00').split(',')
        data_raw = data_raw_string.split(',')

        # print(data_raw_string)
        # print(type(data_raw[1]))
        print(data_raw)




        # if '$VNYMR' == data_raw[0]:
        if data_raw[0] == '$VNYMR':

            # print(data_raw[0:4])
            # print(data_raw[2])
            raw_imu_data = imu_data_raw()




            Yaw = float(data_raw[1])
            Pitch = float(data_raw[2])
            Roll = float(data_raw[3])
            raw_imu_data.Yaw =  Yaw
            raw_imu_data.Pitch = Pitch
            raw_imu_data.Roll = Roll
            # to obtain Yaw Pitch and Roll using euler2quat to converted to quaternion parameters
            q = euler2quat(Yaw, Pitch, Roll , degrees=True)
            qua_x = float(q[0])
            qua_y = float(q[1])
            qua_z =  float(q[2])
            qua_w =  float(q[3])
            imu_data = Imu()
            imu_data.orientation.x = qua_x
            imu_data.orientation.y = qua_y
            imu_data.orientation.z = qua_z
            imu_data.orientation.w = qua_w

            # log 3 accelerometers, 3 angular rate gyros and 3-axis magnetometers data into Imu msg

            imu_data.linear_acceleration.x = float(data_raw[7])
            imu_data.linear_acceleration.y = float(data_raw[8])
            imu_data.linear_acceleration.z = float(data_raw[9])

            imu_data.angular_velocity.x = float(data_raw[10])
            imu_data.angular_velocity.y = float(data_raw[11])
            tmp = data_raw[12]
            tmp_angular_z = tmp.split('*')
            angular_z = tmp_angular_z[0]
            imu_data.angular_velocity.z = float(angular_z)


            mag_data = MagneticField()
            mag_data.magnetic_field.x = float(data_raw[4])
            mag_data.magnetic_field.y = float(data_raw[5])
            mag_data.magnetic_field.z = float(data_raw[6])
            print(data_raw[4:7])
            # print(data)
            # publish imu data and magnetic data
            pub_imu.publish(imu_data)
            pub_mag.publish(mag_data)
            pub_raw.publish(raw_imu_data)






            
if __name__ == '__main__':
    try:

        read_data()
    except rospy.ROSInterruptException:
        pass

    
    
    






    

    





