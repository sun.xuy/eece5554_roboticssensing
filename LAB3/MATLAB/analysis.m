close all;
clc;
clear all;

%import raw data
warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames');

open_field_stationary = readtable('open_field_stationary.csv');
open_field_walking = readtable('open_field_walking.csv');
ISEC_stationary = readtable('stationary_ISEC1.csv');
ISEC_walking = readtable('walking_ISEC1.csv');

utm_easting_open_field_stationary = open_field_stationary.field_utmeasting;
utm_northing_open_field_stationary = open_field_stationary.field_utmnorthing;

utm_easting_ISEC_stationary = ISEC_stationary.field_utmeasting;
utm_northing_ISEC_stationary = ISEC_stationary.field_utmnorthing;

utm_easting_open_field_walking = open_field_walking.field_utmeasting;
utm_northing_open_field_walking = open_field_walking.field_utmnorthing;

utm_easting_ISEC_walking = ISEC_walking.field_utmeasting;
utm_northing_ISEC_walking = ISEC_walking.field_utmnorthing;





%%
% plot the stationary error 
figure;
subplot(1,2,1)
scatter(utm_easting_open_field_stationary,utm_northing_open_field_stationary,'x')
axis equal;
title('stationary data near ISEC')
xlabel('easting[meters]')
ylabel('northing[meters]')
grid on
box on

subplot(1,2,2)
scatter(utm_easting_ISEC_stationary,utm_northing_ISEC_stationary,'x')
axis equal;
title('stationary data in open field')
xlabel('easting[meters]')
ylabel('northing[meters]')
grid on
box on
sgtitle('stationary data in comparison')
%%

% plot the stationary error 
figure;
subplot(2,2,1)
plot(utm_easting_open_field_stationary-utm_easting_open_field_stationary(1))
title('stationary data near ISEC in easting')
grid on
box on
xlabel('time[s]')
ylabel('length[m]')

subplot(2,2,2)
plot(utm_northing_open_field_stationary-utm_northing_open_field_stationary(1))
title('stationary data near ISEC in northing')
grid on
box on
xlabel('time[s]')
ylabel('length[m]')



subplot(2,2,3)
plot(utm_easting_ISEC_stationary-utm_easting_ISEC_stationary(1))
title('stationary data in open field in easting')
grid on
box on
xlabel('time[s]')
ylabel('length[m]')

subplot(2,2,4)
plot(utm_northing_ISEC_stationary-utm_northing_ISEC_stationary(1))
title('stationary data in open field in northing')
xlabel('easting[meters]')
grid on
box on
xlabel('time[s]')
ylabel('length[m]')

sgtitle('stationary data in comparison after subtract the first data of the whole group')



%%
% plot route data

% plot the walking route 
figure;
subplot(1,2,1)
plot(utm_easting_ISEC_walking-utm_easting_ISEC_walking(1),utm_northing_ISEC_walking-utm_northing_ISEC_walking(1))
axis equal;
title('walking data near ISEC')
xlabel('easting[meters]')
ylabel('northing[meters]')
grid on
box on

subplot(1,2,2)

plot(utm_easting_open_field_walking-utm_easting_open_field_walking(1),utm_northing_open_field_walking-utm_northing_open_field_walking(1))
axis equal;
title('walking data in open field')
xlabel('easting[meters]')
ylabel('northing[meters]')
grid on
box on
sgtitle('walking data in comparison')


%%
open_field_stationary_fix_quality = open_field_stationary.field_fix_quality;
open_field_walking_fix_quality = open_field_walking.field_fix_quality;

ISEC_stationary_fix_quality = ISEC_stationary.field_fix_quality;
ISEC_walking_fix_quality = ISEC_walking.field_fix_quality;

figure;
subplot(2,2,1)
plot(open_field_stationary_fix_quality)
grid on
box on
xlabel('time[s]')
ylabel('length[m]')
title('open field stationary fix quality')




subplot(2,2,2)
plot(open_field_walking_fix_quality)
grid on
box on
xlabel('time[s]')
ylabel('length[m]')
title('open field walking fix quality')


subplot(2,2,3)
plot(ISEC_stationary_fix_quality)
grid on
box on
xlabel('time[s]')
ylabel('length[m]')
title('ISEC stationary fix quality')


subplot(2,2,4)
plot(ISEC_walking_fix_quality)
grid on
box on
xlabel('time[s]')
ylabel('length[m]')
title('ISEC stationary fix quality')

sgtitle('fix quality')


%%
start = 1;
open_easting = utm_easting_open_field_stationary(start:end);
open_northing = utm_northing_open_field_stationary(start:end);
ISEC_easting = utm_easting_ISEC_stationary(start:end);
ISEC_northing = utm_northing_ISEC_stationary(start:end);




figure;
subplot(2,2,1)
histogram(open_easting)
grid on
box on
xlabel('easting[m]')
ylabel('samples[times]')
title('utm easting in open field')


subplot(2,2,2)
histogram(open_northing)
grid on
box on
xlabel('easting[m]')
ylabel('samples[times]')
title('utm northing in open field')

subplot(2,2,3)
histogram(ISEC_easting)
grid on
box on
xlabel('easting[m]')
ylabel('samples[times]')
title('utm easting near ISEC')

subplot(2,2,4)
histogram(ISEC_northing)
grid on
box on
xlabel('easting[m]')
ylabel('samples[times]')
title('utm northing near ISEC')

sgtitle('stationary error distribution')






